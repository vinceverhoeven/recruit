/// Import node modules
///////////////////////

/// This one is needed for functionality on nodejs =< 0.10:
require('es6-promise').polyfill();

var gulp = require('gulp'),
    rename = require('gulp-rename'),
    cache = require('gulp-cached'),
    inherit = require('gulp-sass-inheritance'),
    debug = require('gulp-debug'),

    sass = require('gulp-sass'),
    csso = require('gulp-csso'),
    prefix = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    base64 = require('gulp-base64'),
    imagemin = require('gulp-imagemin'),

    concat = require('gulp-concat'),
    uglify = require('gulp-uglify');


/// Variables (files and folders)
/////////////////////////////////

var src = 'public/app/Resources/',
    dest = 'public/web/';

var scss = {
    dir: src + 'scss/',
    all: src + 'scss/**/*.scss',
    files: [
        src + 'scss/*.scss',
        // src + 'scss/components/*.scss'
    ],
    dest: dest + 'css/'
};

var js = {
    all: src + 'js/components/*.js',
    files: [
        src + 'components/jquery/dist/jquery.min.js',
        'node_modules/popper.js/dist/umd/popper.min.js',
        src + 'components/bootstrap/dist/js/bootstrap.min.js',
        src + 'js/components/*.js',
        // src + 'js/components/globals.js',
        // src + 'js/components/!(init)*.js',
        // src + 'js/components/init.js'
    ],
    dest: dest + 'js/'
};

var img = {
    all: src + 'img/*.*',
    dest: dest + 'img/'
};

var font = {
    all: [
        src + 'fonts/*.*',
        src + 'components/fontawesome/fonts/*.*'
    ],
    dest: dest + 'fonts/'
}

var browserlist = ['> 4%', 'last 3 versions', 'Firefox ESR'];


/// Tasks
/////////

gulp.task('cache:clear', function() {
    // console.info(cache.caches);
    cache.caches = {};
});

gulp.task('sass', function() {
    return gulp.src(scss.files)
            // .pipe(cache('sass'))
            // .pipe(inherit({ dir: scss.dir  }))
            // .pipe(sourcemaps.init())
            .pipe(debug({ title: 'Processing: '}))
            .pipe(sass({
                outputStyle: 'compact'
            }).on('error', sass.logError))
            .pipe(base64({
                extensions: ['svg'],
                debug: false
            }))
            .pipe(prefix({
                browsers: browserlist
            }))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(scss.dest));
});

gulp.task('sass:build', function() {
    return gulp.src(scss.files)
            .pipe(sass().on('error', sass.logError))
            .pipe(base64({
                extensions: ['svg']
            }).on('error', function(err) { console.log(err); }))
            .pipe(prefix({
                browsers: browserlist
            }))
            .pipe(csso())
            .pipe(rename({
                suffix: '.min'
            }))
            .pipe(gulp.dest(scss.dest));
});

gulp.task('js', function() {
    return gulp.src(js.files)
            .pipe(concat('app.js'))
            .pipe(gulp.dest(js.dest));
});

gulp.task('js:build', function() {
    return gulp.src(js.files)
            .pipe(concat('app.min.js'))
            .pipe(uglify())
            .pipe(gulp.dest(js.dest));
});

/**
 * @Task Image
 * Run task img
 *
 * Just pipe all images from project folder to public assets folder
 */
gulp.task('img', function () {
    return gulp.src(img.all)
        .pipe(imagemin({
            optimizationLevel: 3,
            progressive: true,
            interlaced: true
        }))
        .pipe(gulp.dest(img.dest));
});

/**
 * @Task Fonts
 * Run task font
 *
 * Just pipe all fonts from project folder to public assets folder
 */
gulp.task('font', function () {
    return gulp.src(font.all)
        .pipe(gulp.dest(font.dest));
});

gulp.task('develop', [
    'cache:clear',
    'sass',
    'js',
    'img',
    'font'
]);

gulp.task('build', [
    'cache:clear',
    'sass:build',
    'js:build'
]);

gulp.task('watch', ['default']);

gulp.task('default', ['develop'], function() {
    gulp.watch(scss.all, ['sass']);
    gulp.watch(js.all, ['js']);
    gulp.watch(img.all, ['img']);
    gulp.watch(font.all, ['font']);
});


/**
 * @Task all
 * Run task all
 *
 * define executable tasks when running "gulp" command
 */
gulp.task('all', ['js', 'sass', 'img', 'font']);