/**
 * Vince Verhoeven
 *
 * @author V. verhoeven <vinceverhoeven@hotmail.com>
 * @copyright For the full copyright or questions, please contact me
 */

function SpreadsheetCreator() {
    this.ss = SpreadsheetApp.getActiveSpreadsheet();
    this.sheet = SpreadsheetApp.getActiveSheet();
    this.newSheet = null;
    this.newSpreadSheet = null;
    this.newFileName = 'excel' + this.ss.getId();
    this.lastRow = null;
    this.lastColumn = null;
    this.valuesToCopyHeaders = null;
    this.valuesToCopyLastUser = null;
}

SpreadsheetCreator.prototype = {
    insertValues: function () {
        //Paste to another sheet from first cell onwards
        this.newSheet.getRange(1, 1, 1, this.lastColumn).setValues(this.valuesToCopyHeaders);
        this.newSheet.getRange(2, 1, this.lastRow, this.lastColumn).setValues(this.valuesToCopyLastUser);
        SpreadsheetApp.flush();
    },
    copyValues: function () {
        // getRange(firstRow,firstColumn,lastRow,lastColumn)
        this.valuesToCopyHeaders = this.sheet.getRange(1, 1, 1, this.lastColumn).getValues();
        this.valuesToCopyLastUser = this.sheet.getRange(this.lastRow, 1, this.lastRow, this.lastColumn).getValues();
    },
    setLastColumnRow: function () {
        this.lastColumn = this.sheet.getLastColumn();
        this.lastRow = this.sheet.getLastRow();
    },
    deleteExisting: function(){
        var files = DriveApp.getFilesByName(this.newFileName);
        if (files.hasNext()) {
            files.next().setTrashed(true)
        }
    },
    createNew: function () {
        this.newSpreadSheet = SpreadsheetApp.create(this.newFileName);
        this.newSheet = this.newSpreadSheet.getActiveSheet();
    },
    createExcel: function() {

        var url = "https://docs.google.com/feeds/download/spreadsheets/Export?key=" +  this.newSpreadSheet.getId()  + "&exportFormat=xlsx";

        var params = {
            method      : "get",
            headers     : {"Authorization": "Bearer " + ScriptApp.getOAuthToken()},
            muteHttpExceptions: true
        };

        this.excel = UrlFetchApp.fetch(url, params).getBlob();
        this.excel.setName(this.newFileName + ".xlsx");
    },
    render: function () {
        this.deleteExisting();
        this.setLastColumnRow();
        this.createNew();
        this.copyValues();
        this.insertValues();
        this.createExcel();
    }
};
function WordCreator(SpreadsheetInstance){
    this.SpreadsheetInstance = SpreadsheetInstance;
    this.newFileName = 'word' + SpreadsheetInstance.ss.getId();
    this.word = null;
    this.Document = null;
    this.styleQuestions = {};
    this.styleAnswers = {};
}

WordCreator.prototype = {
    deleteExisting: function(){
        var files = DriveApp.getFilesByName(this.newFileName);
        if (files.hasNext()) {
            files.next().setTrashed(true)
        }
    },
    createWord: function(){

        var url = "https://docs.google.com/feeds/download/documents/export/Export?id="+ this.Document.getId() +"&exportFormat=docx";

        var params = {
            method      : "get",
            headers     : {"Authorization": "Bearer " + ScriptApp.getOAuthToken()},
            muteHttpExceptions: true
        };

        this.word = UrlFetchApp.fetch(url, params).getBlob();
        this.word.setName(this.newFileName + ".docx");
    },
    create:function (){
        this.Document = DocumentApp.create(this.newFileName);
    },
    setValues:function(){
        var questions = this.SpreadsheetInstance.valuesToCopyHeaders;
        var answers = this.SpreadsheetInstance.valuesToCopyLastUser
        var body =   this.Document.getBody();

        for(n=0;n<=questions[0].length;++n){
            if(questions[0][n]){
                var q = body.appendParagraph(questions[0][n]);
                q.setAttributes(this.styleQuestions);

                var a = body.appendParagraph(answers[0][n]);
                a.setAttributes(this.styleAnswers);
                body.appendParagraph('');
            }
        }
        this.Document.saveAndClose();
    },
    setStyles: function(){
        this.styleQuestions[DocumentApp.Attribute.FONT_FAMILY] = 'Roboto';
        this.styleQuestions[DocumentApp.Attribute.FOREGROUND_COLOR] = '#2870e2';
        this.styleQuestions[DocumentApp.Attribute.BOLD] = true;

        this.styleAnswers[DocumentApp.Attribute.BOLD] = false;
        this.styleAnswers[DocumentApp.Attribute.FOREGROUND_COLOR] = '#000000';
    },
    render: function () {
        this.deleteExisting();
        this.setStyles();
        this.create();
        this.setValues();
        this.createWord();
    }
};


function PdfCreator(WordCreatorInstance) {
    this.WordCreatorInstance = WordCreatorInstance;
    this.newFileName = 'pdf' + WordCreatorInstance.newFileName;
    this.file = null;
}

PdfCreator.prototype = {
    deleteExisting: function(){
        var files = DriveApp.getFilesByName(this.newFileName);
        if (files.hasNext()) {
            files.next().setTrashed(true)
        }
    },
    createNew: function () {
        var docblob = this.WordCreatorInstance.Document.getAs('application/pdf');
        /* Add the PDF extension */
        docblob.setName(this.newFileName);
        this.file = DriveApp.createFile(docblob);
    },
    render: function () {
        this.deleteExisting();
        this.createNew();
    }
};

function MailCreator(spreadsheet, word, pdf){
    this.receipient = 'recruiters.application+production@gmail.com';
    this.subject = 'Form attachments';
    this.word = word;
    this.pdf = pdf;
    this.spreadsheet = spreadsheet;
}

MailCreator.prototype = {
    mail: function(){
        MailApp.sendEmail(
            this.receipient,
            this.subject,
            "", {
                attachments: [this.pdf.file, this.spreadsheet.excel, this.word.word]
            }
        );
    },
    render: function(){
        this.mail();
    }
};


function run() {
    var SpreadsheetInstance = new SpreadsheetCreator();
    SpreadsheetInstance.render();

    var WordCreatorInstance = new WordCreator(SpreadsheetInstance);
    WordCreatorInstance.render();

    var PdfCreatorInstance = new PdfCreator(WordCreatorInstance);
    PdfCreatorInstance.render();

    //mail all files to recruiters.application@gmail.com
    var mailCreatorInstance = new MailCreator(SpreadsheetInstance, WordCreatorInstance, PdfCreatorInstance);
    mailCreatorInstance.render();
}














