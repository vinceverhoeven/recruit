<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 27-Sep-17
 * Time: 16:24
 */

namespace WebcrawlerBundle\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DomCrawler\Crawler;

/**
 * TODO (1) Extend candidate entity with url crawler
 * TODO (2) Get detail information of candidate of url 1
 * TODO (3) Parse information in candidate entity
 * TODO (4) Save unique url/id so it won't parse anymore
 * TODO (5) Refactor in usable service components (Kan ook via bedrijven gaan?)
 */
abstract class WebcrawlerService
{

    /**
     * @var Client
     */
    protected $client;

    protected $website;

    /**
     * @var string
     */
    const INDEED_URL = 'https://www.indeed.com';

    /**
     * @var string
     */
    const INDEED_URL_LIST_VIEW_1 = '/resumes?q=webdeveloper&l=Nederland&co=NL&sort=date&lmd=week';

    /**
     * WebcrawlerService constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param $url string
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function parseUrl($url)
    {
        $response = $this->client->request('GET', $this->getWebsite() . $url);
        return $response;
    }

    /**
     * @param Response $response
     * @param $options array
     * @return array
     */
    public function parseWebPageIntoLinks($response, $options)
    {
        $crawlableLinks = array();
        $html = $response->getBody()->getContents();
        $crawler = new Crawler($html);

        $filter = $crawler->filter($options['selector']);

        foreach ($filter as $i => $content) {

            $crawlerFilter = new Crawler($content);
            $crawlableLinks[$i] = array(
                "url" => $this->getWebsite() . $crawlerFilter->filter('a')->attr('href')
            );
        }
        return $crawlableLinks;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param $website string
     */
    public function setWebsite($website)
    {
        $this->website = $website;
    }
}