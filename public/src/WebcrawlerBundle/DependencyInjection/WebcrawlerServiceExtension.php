<?php
/**
 * Vince Verhoeven
 *
 * @author V. verhoeven <vinceverhoeven@hotmail.com>
 * @copyright For the full copyright or questions, please contact me
 */
namespace WebcrawlerBundle\DependencyInjection;

use WebcrawlerBundle\Service\WebcrawlerService;
use Symfony\Component\DomCrawler\Crawler;

class WebcrawlerServiceExtension extends WebcrawlerService
{
    /**
     * @param $guzzleResponse \Psr\Http\Message\ResponseInterface
     */
    public function parseIndeedDetailPage($guzzleResponse)
    {

    }

    /**
     * @param array $options
     */
    public function createCandidateEntity($options = array())
    {

    }
}