<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 28-Sep-17
 * Time: 12:44
 */

namespace WebcrawlerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="webcrawler_content")
 * @ORM\Entity(repositoryClass="WebcrawlerBundle\Entity\Repository\WebCrawlerContentRepository")

 */
class WebCrawlerContent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="WebCrawler", inversedBy="links")
     */
    private $crawledUrl;

    /**
     * @var string
     * @ORM\Column(name="parsed_link", nullable=true, type="string", nullable=false)
     */
    private $parsed_link;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_parsed", nullable=true, type="integer", options={"default":0})
     */
    private $numberParsed = 0;

    /**
     * Timestamp of creation
     *
     * @ORM\Column(name="date_added", type="datetime")
     *
     * @Gedmo\Timestampable(on="create")
     */
    private $dateAdded;

    /**
     * Timestamp of update
     *
     * @ORM\Column(name="date_updated", type="datetime")
     *
     * @Gedmo\Timestampable(on="update")
     */
    private $dateUpdated;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set crawledLink
     *
     * @param string $crawledLink
     *
     * @return WebCrawlerContent
     */
    public function setCrawledLink($crawledLink)
    {
        $this->crawled_link = $crawledLink;

        return $this;
    }

    /**
     * Set numberParsed
     *
     * @param integer $numberParsed
     *
     * @return WebCrawlerContent
     */
    public function setNumberParsed($numberParsed)
    {
        $this->numberParsed = $numberParsed;

        return $this;
    }

    /**
     * Get numberParsed
     *
     * @return integer
     */
    public function getNumberParsed()
    {
        return $this->numberParsed;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return WebCrawlerContent
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return WebCrawlerContent
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * Set crawledId
     *
     * @param \WebcrawlerBundle\Entity\WebCrawler $crawledId
     *
     * @return WebCrawlerContent
     */
    public function setCrawledId(\WebcrawlerBundle\Entity\WebCrawler $crawledId = null)
    {
        $this->crawledId = $crawledId;

        return $this;
    }

    /**
     * Set crawledUrl
     *
     * @param integer $crawledUrl
     *
     * @return WebCrawlerContent
     */
    public function setCrawledUrl($crawledUrl)
    {
        $this->crawledUrl = $crawledUrl;

        return $this;
    }

    /**
     * Get crawledUrl
     *
     * @return integer
     */
    public function getCrawledUrl()
    {
        return $this->crawledUrl;
    }

    /**
     * Set parsedLink
     *
     * @param string $parsedLink
     *
     * @return WebCrawlerContent
     */
    public function setParsedLink($parsedLink)
    {
        $this->parsed_link = $parsedLink;

        return $this;
    }

    /**
     * Get parsedLink
     *
     * @return string
     */
    public function getParsedLink()
    {
        return $this->parsed_link;
    }
}
