<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 28-Sep-17
 * Time: 17:46
 */

namespace WebcrawlerBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class WebCrawlerContentRepository extends EntityRepository
{
    /**
     * @param $id int
     * @return array
     */
    public function getLinksByWebsiteId($id)
    {
        $q = $this->createQueryBuilder('w')->select()
            ->where('w.crawledUrl = :id')->setParameter('id', $id);
        return array(
            "array" => $q->getQuery()->getArrayResult(),
            "entity" => $q->getQuery()->getResult());
    }
}