<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 28-Sep-17
 * Time: 19:20
 */

namespace WebcrawlerBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class WebCrawlerRepository extends EntityRepository
{
    /**
     * @return array
     */

    public function findAllCrawledPages()
    {
        $q = $this->createQueryBuilder('w')->select();
        return array(
            "array" => $q->getQuery()->getArrayResult(),
            "entity" => $q->getQuery()->getResult());
    }
}