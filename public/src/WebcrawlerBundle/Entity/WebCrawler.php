<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 27-9-17
 * Time: 22:41
 */

namespace WebcrawlerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="webcrawler")
 * @ORM\Entity(repositoryClass="WebcrawlerBundle\Entity\Repository\WebCrawlerRepository")

 */
class WebCrawler
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="crawled_url", nullable=true, type="string")
     */
    private $crawledUrl;

    /**
     *  @ORM\OneToMany(targetEntity="WebCrawlerContent", mappedBy="crawledUrl")
     */
    private $links;

    /**
     * Timestamp of creation
     *
     * @ORM\Column(name="date_added", type="datetime")
     *
     * @Gedmo\Timestampable(on="create")
     */
    private $dateAdded;

    /**
     * Timestamp of update
     *
     * @ORM\Column(name="date_updated", type="datetime")
     *
     * @Gedmo\Timestampable(on="update")
     */
    private $dateUpdated;

    /**
     * @var integer
     *
     * @ORM\Column(name="number_crawled", nullable=true, type="integer", options={"default":0})
     */
    private $numberCrawled = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="log", nullable=true, type="text")
     */
    private $log;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set crawledUrl
     *
     * @param string $crawledUrl
     *
     * @return WebCrawler
     */
    public function setCrawledUrl($crawledUrl)
    {
        $this->crawledUrl = $crawledUrl;

        return $this;
    }

    /**
     * Get crawledUrl
     *
     * @return string
     */
    public function getCrawledUrl()
    {
        return $this->crawledUrl;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return WebCrawler
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set log
     *
     * @param string $log
     *
     * @return WebCrawler
     */
    public function setLog($log)
    {
        $this->log = $log;

        return $this;
    }

    /**
     * Get log
     *
     * @return string
     */
    public function getLog()
    {
        return $this->log;
    }

    /**
     * Set dateUpdated
     *
     * @param \DateTime $dateUpdated
     *
     * @return WebCrawler
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;

        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return \DateTime
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->links = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add link
     *
     * @param \WebcrawlerBundle\Entity\WebCrawlerContent $link
     *
     * @return WebCrawler
     */
    public function addLink(\WebcrawlerBundle\Entity\WebCrawlerContent $link)
    {
        $this->links[] = $link;

        return $this;
    }

    /**
     * Remove link
     *
     * @param \WebcrawlerBundle\Entity\WebCrawlerContent $link
     */
    public function removeLink(\WebcrawlerBundle\Entity\WebCrawlerContent $link)
    {
        $this->links->removeElement($link);
    }

    /**
     * Get links
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * Set numberCrawled
     *
     * @param integer $numberCrawled
     *
     * @return WebCrawler
     */
    public function setNumberCrawled($numberCrawled)
    {
        $this->numberCrawled = $numberCrawled;

        return $this;
    }

    /**
     * Get numberCrawled
     *
     * @return integer
     */
    public function getNumberCrawled()
    {
        return $this->numberCrawled;
    }

    /**
     * Set links
     *
     * @param \WebcrawlerBundle\Entity\WebCrawlerContent $links
     *
     * @return WebCrawler
     */
    public function setLinks(\WebcrawlerBundle\Entity\WebCrawlerContent $links = null)
    {
        $this->links = $links;

        return $this;
    }
}
