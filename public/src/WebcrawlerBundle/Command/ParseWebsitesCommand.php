<?php
/*
 * (c) Vince Verhoeven <vinceverhoeven@hotmail.com>
 */

namespace WebcrawlerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use WebcrawlerBundle\Entity\WebCrawler;
use WebcrawlerBundle\Entity\WebCrawlerContent;

class ParseWebsitesCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('recruit:parse:websites')
            ->setDescription('Parses inbox argument -dev or -prod')
            ->addArgument('mode', InputArgument::REQUIRED, 'dev or prod. Dev will not save candidates');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            '===== Crawling websites ====='
        ]);

        $webCrawler = $this->getContainer()->get('webcrawler_bundle.webcrawler.extension');
        $webCrawler->setWebsite($webCrawler::INDEED_URL);

        try {
            // get list view with maybe new candidates we always parse this one
            $response = $webCrawler->parseUrl($webCrawler::INDEED_URL_LIST_VIEW_1);

            if ($response->getStatusCode() == 200) {
                // check website
                $foundLinks = $webCrawler->parseWebPageIntoLinks($response, array('selector' => 'a.app_link'));
                // check database
                $parsedPages = $this->getParsedPages($webCrawler->getWebsite() . $webCrawler::INDEED_URL_LIST_VIEW_1);

                $foundLinksCompared = $this->compareLinks($foundLinks, $parsedPages);
                dump($foundLinksCompared);
                //$this->updateWebcrawlEntity($webCrawler->getWebsite() . $webCrawler::INDEED_URL_LIST_VIEW_1, $crawlableLinks);
                die;
                // the links are unique so lets see if we parsed them already
//                foreach ($crawlableLinks as $link) {
//                    $shouldIndex = $this->updateWebcrawlEntity($link['url']);
//                    dump($shouldIndex);
//
//                    if ($shouldIndex) {
//                        $response = $webCrawler->parseUrl($webCrawler::INDEED_URL_LIST_VIEW_1);
//                    }
//                    //$response = $webCrawler->parseUrl($link['url']);
//                }
            }

        } catch (\Exception $e) {
            // TODO put in error log of crons
            dump($e->getMessage());
        }
    }


    protected function getParsedPages($listViewUrl)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        return $webCrawlerEntity = $doctrine->getRepository(WebCrawler::class)
            ->findOneBy(array('crawledUrl' => $listViewUrl));
    }

    protected function compareLinks($foundLinks, $parsedLinks)
    {
        foreach ($foundLinks as $key => $link) {
            $foundit = array_search($link['url'], array_column($parsedLinks["array"], 'parsed_link'));

            if (is_int($foundit)) {
                $foundLinks[$key]['index'] = false;
            }

            if ($foundit === false) {
                $foundLinks[$key]['index'] = true;
            }
        }
        return $foundLinks;
    }

    /**
     * @param $url string
     * @return bool
     */
    protected function updateWebcrawlEntity($url, $links = array(), $log = null)
    {
        // TODO refactor
        $shouldIndex = true;
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();

        $webCrawlerEntity = $doctrine->getRepository(WebCrawler::class)
            ->findOneBy(array('crawledUrl' => $url));
        dump($doctrine->getRepository(WebCrawlerContent::class)
            ->getLinksByWebsiteId($webCrawlerEntity->getId()));
        die;


        if ($webCrawlerEntity) {
            $webCrawlerEntity->setNumberCrawled($webCrawlerEntity->getNumberCrawled() + 1);
            $webCrawlerEntity->setLog($log);


            $shouldIndex = false;

            // Loop through all contentEntity of parsed url
            foreach ($links as $link) {
                //$foundit = $key = array_search($link['url'], array_column($webCrawlerEntity->getLinks()->getValues(), '-parsed_link'), false);
//                $collection = $webCrawlerEntity->getLinks();
//                dump($collection->getValues());

//                if($foundit === false){
//                    var_dump($foundit);
//                }
                die;
//                foreach ($links as $link) {
//                    if ($webCrawlerContent->getParsedLink() == $link['url']) {
//                        $webCrawlerContent->setNumberParsed($webCrawlerContent->getNumberParsed() + 1);
//                    }
//                    if ($webCrawlerContent->getParsedLink() !== $link['url']) {
//                        $webCrawlerContent = new WebCrawlerContent();
//                        $webCrawlerContent->setParsedLink($link['url']);
//                        $webCrawlerContent->setNumberParsed(1);
//                        $webCrawlerContent->setCrawledUrl($webCrawlerEntity);
//                    }
//                }

                //$em->persist($webCrawlerContent);
            }
            die;

            // $links = $product->getLink();

            // update webcrawlerContent

        } else {
            $webCrawlerEntity = new WebCrawler();
            $webCrawlerEntity->setLog($log);
            $webCrawlerEntity->setCrawledUrl($url);
            $webCrawlerEntity->setNumberCrawled(1);

            foreach ($links as $link) {
                $webCrawlerContent = new WebCrawlerContent();
                $webCrawlerContent->setParsedLink($link['url']);
                $webCrawlerContent->setNumberParsed(1);
                $webCrawlerContent->setCrawledUrl($webCrawlerEntity);
                $em->persist($webCrawlerContent);
            }

            // new webcrawlerContent
        }

        $em->persist($webCrawlerEntity);
        $em->flush();
        return $shouldIndex;
    }

    public function createNew()
    {

    }

}