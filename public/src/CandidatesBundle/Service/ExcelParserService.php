<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 11-Aug-17
 * Time: 18:59
 */

namespace CandidatesBundle\Service;

use CandidatesBundle\Entity\Candidates;
use CandidatesBundle\Entity\CandidatesImport;
use Symfony\Component\DependencyInjection\Container;

class ExcelParserService
{
    private $container;

    // keep string low characters
    const NOT_CALLABLE = array('id');

    const ALLOWED_EXTENSIONS = array('xls', 'xlsx');

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function parseExcel(CandidatesImport $file, $columnNames)
    {
        $fileService = $this->container->get('file.upload.service');
        $excel = $this->container->get('phpexcel')->createPHPExcelObject($fileService->getAbsolutePath());
        $methods = array();
        $em = $this->container->get('doctrine.orm.default_entity_manager');

        foreach ($excel->getWorksheetIterator() as $worksheet) {
            foreach ($worksheet->getRowIterator() as $row) {
                $candidate = new Candidates();

                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set

                $saveInDatabase = true;
                foreach ($cellIterator as $cell) {

                    if (!is_null($cell)) {
                        // (1) get column and set it as key in methods array
                        $column = preg_replace('/[0-9]+/', '', $cell->getCoordinate());

                        if ($row->getRowIndex() == 1) {
                            $methodName = 'set' . ucfirst(strtolower($cell->getCalculatedValue()));

                            // (2) get row and parse it to callable methods only first row in excel
                            if (is_callable(array($candidate, $methodName))) {
                                $methods[$column] = $methodName;
                            }
                        }

                        if ($row->getRowIndex() > 1) {
                            if (!in_array('setName', $methods, true) || !in_array('setName', $methods, true) ) {
                                $file->setFileUsed(0);
                                $file->setDeleted(1);
                                $em->persist($file);
                                $em->flush();
                                break;
                            }

                            if (isset($methods[$column])) {
                                // (3) use the method array as callable functions for each row
                                if($methods[$column] == 'setDateadded' || $methods[$column] == 'setDatedeleted'){
                                    $date = \DateTime::createFromFormat('Y-m-d H:i:s',$cell->getCalculatedValue());
                                    $candidate->{$methods[$column]}($date);
                                }else{
                                    $candidate->{$methods[$column]}($cell->getCalculatedValue());
                                }


                                // (4) if row not containing name or job then we will not save row in database
                                if ($methods[$column] == 'setName'){
                                    if(is_null($cell->getCalculatedValue())){
                                        $saveInDatabase = false;
                                    }
                                }

                            }
                        }
                    }
                }
                // (5) save row in database and set fileimportid for revert possibilities
                if ($row->getRowIndex() > 1 && $saveInDatabase) {
                    $candidate->setFileImportId($file->getId());
                    $em->persist($candidate);
                    $em->flush();
                }
            }
        }
    }

}