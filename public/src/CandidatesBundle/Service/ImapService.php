<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 29-8-17
 * Time: 16:16
 */

namespace CandidatesBundle\Service;


use CandidatesBundle\Entity\Candidates;
use CandidatesBundle\Entity\CandidatesGoogleForms;
use Symfony\Component\DependencyInjection\Container;

class ImapService
{
    protected $development_mode;

    private $container;

    private $gmail_username;

    private $gmail_password;

    private $dev_inbox;

    private $dev_processed;

    private $prod_inbox;

    private $prod_processed;

    public $inbox;

    public $emails;

    public function __construct(
        Container $container, $gmail, $gmail_password,
        $dev_inbox, $dev_processed, $prod_inbox, $prod_processed)
    {
        $this->container = $container;
        $this->gmail_username = $gmail;
        $this->gmail_password = $gmail_password;
        $this->dev_inbox = $dev_inbox;
        $this->dev_processed = $dev_processed;
        $this->prod_inbox = $prod_inbox;
        $this->prod_processed = $prod_processed;
    }

    public function moveMail($email)
    {
        if ($this->getDevelopmentMode()) {
            if ($this->getDevelopmentMode() === 'dev') {
                imap_mail_move($this->getInbox(), $email, $this->dev_processed);
            }
            if ($this->getDevelopmentMode() === 'prod') {
                imap_mail_move($this->getInbox(), $email, $this->prod_processed);
            }
        }
    }

    public function setAsSeen($email)
    {
        imap_setflag_full($this->getInbox(), $email, "\\Seen", ST_UID);
    }

    private function setInbox()
    {
        if ($this->getDevelopmentMode()) {
            if ($this->getDevelopmentMode() === 'dev') {
                $hostname = '{imap.gmail.com:993/imap/ssl}' . $this->dev_inbox;
            }
            if ($this->getDevelopmentMode() === 'prod') {
                $hostname = '{imap.gmail.com:993/imap/ssl}' . $this->prod_inbox;
            }
            $this->inbox = imap_open($hostname, $this->gmail_username, $this->gmail_password)
            or imap_last_error();
        } else {
            dump('no dev mode');
        }
    }

    public function parseGmailInbox()
    {
        $this->setInbox();
        $this->setEmails();
        return $this->getEmails();
    }

    protected function createGoogleFormUser()
    {
        $candidate = new Candidates();
        $candidate->setName('Google Form User');
        $em = $this->container->get('doctrine.orm.default_entity_manager');
        $em->persist($candidate);
        $em->flush();

        return $candidate;
    }

    protected function createCandidatesGoogleForms($attachment, $googleForms)
    {
        if ($attachment['filetype'] == 'PDF') {
            $googleForms->setPathPdf($attachment['name']);
        }
        if ($attachment['filetype'] == 'VND.OPENXMLFORMATS-OFFICEDOCUMENT.SPREADSHEETML.SHEET') {
            $googleForms->setPathExcel($attachment['name']);
        }
        if ($attachment['filetype'] == 'VND.OPENXMLFORMATS-OFFICEDOCUMENT.WORDPROCESSINGML.DOCUMENT') {
            $googleForms->setPathWord($attachment['name']);
        }
    }

    public function downloadAttachments($email_number)
    {
        // (5) Get image src in view
        // (6) Get pdf src in view

        /* get mail structure */
        $inbox = $this->getInbox();
        $structure = imap_fetchstructure($inbox, $email_number);
        $fileService = $this->container->get('file.upload.service');
        $attachments = array();

        /* if any attachments found... */
        if (isset($structure->parts) && count($structure->parts)) {
            // files are found so lets create new google form user
            $candidate = $this->createGoogleFormUser();
            $googleForms = new CandidatesGoogleForms();

            $googleForms->setUser($candidate);

            for ($i = 0; $i < count($structure->parts); $i++) {
                $attachments[$i] = array(
                    'is_attachment' => false,
                    'filename' => '',
                    'name' => '',
                    'attachment' => '',
                    'filetype' => ''
                );

                if ($structure->parts[$i]->ifdparameters) {
                    foreach ($structure->parts[$i]->dparameters as $object) {
                        if (strtolower($object->attribute) == 'filename') {
                            $attachments[$i]['is_attachment'] = true;
                            $attachments[$i]['filename'] = $object->value;
                            $attachments[$i]['filetype'] = $structure->parts[$i]->subtype;
                        }
                    }
                }

                if ($structure->parts[$i]->ifparameters) {
                    foreach ($structure->parts[$i]->parameters as $object) {
                        if (strtolower($object->attribute) == 'name') {
                            $attachments[$i]['is_attachment'] = true;
                            $attachments[$i]['name'] = $object->value;
                        }
                    }
                }

                if ($attachments[$i]['is_attachment']) {
                    $attachments[$i]['attachment'] = imap_fetchbody($inbox, $email_number, $i + 1);

                    /* 3 = BASE64 encoding */
                    if ($structure->parts[$i]->encoding == 3) {
                        $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
                    } /* 4 = QUOTED-PRINTABLE encoding */
                    elseif ($structure->parts[$i]->encoding == 4) {
                        $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
                    }
                }
            }

            /* iterate through each attachment and save it */
            foreach ($attachments as $attachment) {
                if ($attachment['is_attachment'] == 1) {
                    $fileService->setUploadDir($fileService::DIR_GOOGLE_FORM_ATTACHMENTS);
                    $fileService->uploadEmailAttachment($attachment, $candidate->getId());
                    $this->createCandidatesGoogleForms($attachment, $googleForms);
                }
            };
            // TODO refactor

            $imageName = $this->pdfAttachmentToImage($googleForms);
            $googleForms->setPathImage($imageName);
            // save google form user
            $em = $this->container->get('doctrine.orm.default_entity_manager');
            $em->persist($googleForms);
            $em->flush();
        }
    }

    protected function pdfAttachmentToImage($googleForms)
    {
        // TODO refactor

        $imageService = $this->container->get('image.service');
        $fileService = $this->container->get('file.upload.service');
        $pdfPath = $fileService->getUserUploadDir() . "/" . $googleForms->getPathPdf();
        $imageName = $imageService->pdfToImage($fileService->getUserUploadDir(), $pdfPath);
        return $imageName;
    }

    public function setEmails()
    {
        $this->emails = imap_search($this->getInbox(), 'FROM ' . $this->gmail_username);
    }

    /**
     * @return mixed
     */
    public function getInbox()
    {
        return $this->inbox;
    }

    /**
     * @return mixed
     */
    public function getEmails()
    {
        return $this->emails;
    }


    /**
     * @return mixed
     */
    public function getDevelopmentMode()
    {
        return $this->development_mode;
    }

    /**
     * @param mixed $development_mode
     */
    public function setDevelopmentMode($development_mode)
    {
        $this->development_mode = $development_mode;
    }

}