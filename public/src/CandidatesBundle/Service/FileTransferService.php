<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 11-Aug-17
 * Time: 18:59
 */

namespace CandidatesBundle\Service;


use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class FileTransferService
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    protected function createFileName($name, $extension)
    {
        $date = date('d-m-Y_H-i-s');
        $filename = $name . '_' . $date . $extension;
        return $filename;
    }

    public function createCandidateExcel($candidates, $columnNames, $savePath)
    {
        $phpExcelObject = $this->container->get('phpexcel')->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("Vince Verhoeven")
            ->setLastModifiedBy("(c) Vince Verhoeven")
            ->setTitle("Export van alle Kandidaten")
            ->setSubject("Export van alle Kandidaten")
            ->setDescription("selectie van kandidaten tot 26 augustus 2017");


        $phpExcelObject->setActiveSheetIndex(0);
        $abc = 'A';
        foreach ($columnNames as $columnName) {
            $phpExcelObject->getActiveSheet()
                ->setCellValue($abc . 1, $columnName);
            $abc++;
        }

        $lignes = 2;
        foreach ($candidates as $candidate) {
            $phpExcelObject->getActiveSheet()
                ->setCellValue('A' . $lignes, $candidate->getId())
                ->setCellValue('B' . $lignes, $candidate->getName())
                ->setCellValue('C' . $lignes, $candidate->getJob())
                ->setCellValue('D' . $lignes, $candidate->getApproached())
                ->setCellValue('E' . $lignes, $candidate->getLevel())
                ->setCellValue('F' . $lignes, $candidate->getSkills())
                ->setCellValue('G' . $lignes, $candidate->getEmail())
                ->setCellValue('H' . $lignes, $candidate->getMobile())
                ->setCellValue('I' . $lignes, $candidate->getCity())
                ->setCellValue('J' . $lignes, $candidate->getProvince())
                ->setCellValue('K' . $lignes, $candidate->getCountry())
                ->setCellValue('L' . $lignes, $candidate->getLinkedinUrl())
                ->setCellValue('M' . $lignes, $candidate->getPlatform())
                ->setCellValue('N' . $lignes, $candidate->getFileImportId())
                ->setCellValue('O' . $lignes, $candidate->getAddedCompany())
                ->setCellValue('P' . $lignes, $candidate->getNote())
                ->setCellValue('Q' . $lignes, $candidate->getDateAdded())
                ->setCellValue('R' . $lignes, $candidate->getDeleted())
                ->setCellValue('S' . $lignes, $candidate->getDateDeleted());
            $lignes++;
        }


        $phpExcelObject->getActiveSheet()->setTitle('Simple');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $phpExcelObject->setActiveSheetIndex(0);
        $writer = $this->container->get('phpexcel')->createWriter($phpExcelObject, 'Excel5');

        $fileName = $this->createFileName('kandidaten-export', '.xls');
        $fullPath = $savePath . '/' . $fileName;
        $writer->save($fullPath);

        // not return full path for security reasons
        return 'web/export/candidates/' . $fileName;
    }
}