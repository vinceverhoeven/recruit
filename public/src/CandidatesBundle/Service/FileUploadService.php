<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 10-9-17
 * Time: 15:04
 */

namespace CandidatesBundle\Service;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploadService
{
    private $container;

    /**
     * @var UploadedFile
     */
    private $file;

    /**
     * @var string
     */
    protected $fileName;

    /**
     * @var array
     */
    protected $pathInfo;

    protected $exportDir;

    public $userUploadDir;

    protected $extension;
    /**
     * Must be constant of this class
     * @var string
     */
    protected $uploadDir;

    const DIR_EXCEL_TRANSFER = 'ExcelTransfer';

    const DIR_GOOGLE_FORM_ATTACHMENTS = 'GoogleForms';

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    protected function setFileName()
    {
        if ($this->extensionCheck()) {
            $this->fileName = sha1(uniqid(mt_rand(), true)) . $this->getExtension();
        } else {
            $this->fileName = sha1(uniqid(mt_rand(), true));
        }
    }

    public function extensionCheck()
    {
        if (isset($this->extension) && !empty($this->extension)) {
            return true;
        }
    }

    public function allowedExtensionCheck($allowedExtensionArray)
    {
        if (in_array($this->getExtension(), $allowedExtensionArray, true)) {
            return true;
        }
    }

    public function upload()
    {
        // the file property can be empty if the field is not required
        if (null === $this->getFile()) {
            return;
        }

        $this->setFileName();

        // move takes the target directory and then the
        // target filename to move to
        $this->getFile()->move(
            $this->getUploadRootDir(),
            $this->getFileName()
        );

    }

    public function getAbsolutePath()
    {
        return null === $this->fileName
            ? null
            : $this->getUploadRootDir() . '/' . $this->fileName;
    }

    public function getWebPath()
    {
        return null === $this->fileName
            ? null
            : $this->getUploadDir() . '/' . $this->fileName;
    }

    public function getUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../web/uploads/' . $this->getUploadDir();
    }

    public function getExportRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../web/export';
    }

    public function getExportDir()
    {
        return $this->exportDir;
    }

    public function setUserUploadDir($userId)
    {
        $this->userUploadDir = $this->getUploadRootDir() . "/" . $userId;
    }

    public function getUserUploadDir()
    {
        return $this->userUploadDir;
    }

    protected function getUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return $this->uploadDir;
    }

    /**
     * @param string $uploadDir
     */
    public function setUploadDir($uploadDir)
    {
        $this->uploadDir = $uploadDir;
    }

    public function getMimeType($file)
    {
        return (finfo_file(finfo_open(FILEINFO_MIME_TYPE), $file));
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null, $uploadDir)
    {
        $this->file = $file;
        $this->setPathInfo();
        $this->setUploadDir($uploadDir);
        $this->setExtension($this->getPathInfo()['extension']);
    }

    /**
     * @param $attachment
     * @param $userId
     */
    public function uploadEmailAttachment($attachment, $userId)
    {
        dump($userId);
        $this->setUserUploadDir($userId);
        if (!is_dir($this->getUserUploadDir())) {
            mkdir($this->getUserUploadDir(), 0755, true);
        }
        $fp = fopen($this->getUserUploadDir() . '/' . $attachment['name'], "w+");
        fwrite($fp, $attachment['attachment']);
        fclose($fp);
    }

    public function saveExportCandidateExcel()
    {
        $this->setExportDir('candidates');
        $savePath = $this->getExportRootDir() . '/' . $this->getExportDir();
        if (!is_dir($savePath)) {
            mkdir($savePath, 0755, true);
        }

        return $savePath;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return mixed
     */
    public function getPathInfo()
    {
        return $this->pathInfo;
    }


    public function setPathInfo()
    {
        $this->pathInfo = pathinfo($this->getFile()->getClientOriginalName());
    }

    /**
     * @return mixed
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param mixed $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }

    /**
     * @param mixed $exportDir
     */
    public function setExportDir($exportDir)
    {
        $this->exportDir = $exportDir;
    }

}