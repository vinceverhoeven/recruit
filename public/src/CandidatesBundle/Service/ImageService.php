<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 21-Sep-17
 * Time: 21:24
 */

namespace CandidatesBundle\Service;

class ImageService
{

    protected $imagick;

    const FILENAME = 'userForm.jpg';

    public function __construct()
    {
        $this->imagick = new \Imagick();
    }

    public function pdfToImage($userUploadDir, $pdfPath)
    {
        $this->imagick->setResolution(800, 800);
        $this->imagick->readimage($pdfPath);
        $this->imagick->setImageFormat('jpeg');

        // save image
        $this->imagick->writeImage($userUploadDir . "/" . self::FILENAME);
        $this->clearAndDestroy();
        return self::FILENAME;
    }

    private function clearAndDestroy()
    {
        $this->imagick->clear();
        $this->imagick->destroy();
    }
}