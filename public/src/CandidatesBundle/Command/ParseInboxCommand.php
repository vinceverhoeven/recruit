<?php
/*
 * (c) Vince Verhoeven <vinceverhoeven@hotmail.com>
 */

namespace CandidatesBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ParseInboxCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('recruit:inbox:parse')
            ->setDescription('Parses inbox argument -dev or -prod')
            ->addArgument('mode', InputArgument::REQUIRED, 'dev or prod');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln([
            'Parse inbox in ' . $input->getArgument('mode') . 'mode',
            '============',
            '',
        ]);
        $imapService = $this->getContainer()->get('imap.service');

        try {
            $imapService->setDevelopmentMode($input->getArgument('mode'));
            $emails = $imapService->parseGmailInbox();
        } catch (\Exception $e) {
            dump($e->getMessage());
        }


        if (isset($emails) && !empty($emails)) {
            $output->writeln('number of emails: ' . count($emails));

            /* put the newest emails on top */
            rsort($emails);

            /* for every email... */
            foreach ($emails as $email_number) {
                $imapService->downloadAttachments($email_number);
                $imapService->setAsSeen($email_number);
                $imapService->moveMail($email_number);
            }
        }

    }

}