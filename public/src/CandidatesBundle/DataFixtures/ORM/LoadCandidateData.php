<?php

/*
 * (c) Vince Verhoeven <vinceverhoeven@hotmail.com>
 */


namespace CandidatesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use CandidatesBundle\Entity\Candidates;

class LoadCandidateData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $candidate = new Candidates();
        $candidate->setName('Vince Verhoeven');
        $candidate->setCity('Breda');
        $candidate->setEmail('vinceverhoeven@hotmail.com');
        $candidate->setJob('Full stack developer');
        $candidate->setLevel('junior');
        $candidate->setApproached(0);
        $candidate->setSkills('php symfony css js angularjs angular2');
        $candidate->setMobile('0622624986');
        $candidate->setAddedCompany('Google');
        $candidate->setLinkedinUrl('https://www.linkedin.com/in/vinceverhoeven/');
        $candidate->setProvince('Noord-Brabant');
        $candidate->setCountry('Nederland');
        $candidate->setNote('Dit is een test note');
        $manager->persist($candidate);
        $manager->flush();
    }
}