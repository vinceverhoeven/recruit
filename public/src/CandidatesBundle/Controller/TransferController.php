<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 11-8-17
 * Time: 17:04
 */

namespace CandidatesBundle\Controller;

use CandidatesBundle\Entity\Candidates;
use CandidatesBundle\Entity\CandidatesImport;
use CandidatesBundle\Form\ImportExcelType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolation;


class TransferController extends Controller
{
    /**
     * View of import/export csv candidates
     *
     * @Route("/candidates/transfer", name="candidates_transfer")
     */
    public function IndexAction()
    {
        $form = $this->createForm(ImportExcelType::class);

        $uploadedFiles = $this->getDoctrine()
            ->getRepository(CandidatesImport::class)->findAllSortDate();

        return $this->render('CandidatesBundle:modals:candidates_transfer_modal.html.twig', array(
            'transferForm' => $form->createView(),
            'uploadedFiles' => $uploadedFiles
        ));
    }

    /**
     * Creates Excel export of current Database candidates (AJAX call)
     *
     * @Route("/candidates/transfer/export", name="candidates_transfer_export")
     * @Method("GET")
     * @param Request $request
     * @return string
     */
    public function exportAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $candidates = $em->getRepository(Candidates::class)->filterCandidate($request, false);
        $candidates = $candidates->getQuery()->getResult();

        // Get columnnames from entity for creating the excel
        $columnNames = $em->getClassMetadata(Candidates::class)->getFieldNames();

        $fileService = $this->container->get('file.upload.service');
        $savePath = $fileService->saveExportCandidateExcel();

        $fileTransferService = $this->container->get('file.transfer.service');
        $filePath = $fileTransferService->createCandidateExcel($candidates, $columnNames, $savePath);

        return new JsonResponse(array(
            'code' => 200,
            'message' => $filePath,
            'errors' => array('errors' => array(''))),
            200);
    }

    /**
     * Imports excel file and puts in database (AJAX call)
     *
     * @Route("/candidates/transfer/import", name="candidates_transfer_import")
     * @Method("POST")
     * @param Request $request
     * @return string
     */
    public function importAction(Request $request)
    {
        $file = new CandidatesImport();
        $form = $this->createForm(ImportExcelType::class, $file);
        $form->handleRequest($request);
        $errors = array();

        if ($form->isSubmitted()) {
            $fileService = $this->container->get('file.upload.service');

            $validator = $this->get('validator');
            $errorsValidator = $validator->validate($file);

            foreach ($errorsValidator as $error) {
                array_push($errors, $error->getMessage());
            }

            //add custom vali: xlsx and zip not seem to be different by mimeTypes
            if (count($errors) == 0 && $fileService->extensionCheck()
                && $fileService->allowedExtensionCheck(array('xls', 'xlsx'))) {
                array_push($errors, 'Bestand is geen excel type (.xls of .xlsx)');
            }

            if (count($errors) == 0) {
                $fileService->setFile($file->getFile(), $fileService::DIR_EXCEL_TRANSFER);

                $fileService->upload();

                $file->setSize($fileService->getFile()->getSize());
                $file->setName($fileService->getFile()->getClientOriginalName());
                $file->setPath($fileService->getFileName());

                $em = $this->getDoctrine()->getManager();
                $em->persist($file);
                $em->flush();

                $columnNames = $em->getClassMetadata(Candidates::class)->getFieldNames();

                $parser = $this->container->get('excel.parser.service');
                $parser->parseExcel($file, $columnNames);

                // TODO return file info
                return new JsonResponse(array(
                    'code' => 200,
                    'message' => 'kandidaat toegevoegd',
                    'errors' => array('errors' => array(''))),
                    200);
            }
        }
        return new JsonResponse(array(
            'code' => 400,
            'message' => 'error',
            'errors' => array('errors' => $errors)),
            400);
    }


    /**
     * Reverts all candidate data with requested file id (AJAX post call)
     *
     * @Route("/candidates/transfer/revert", name="candidates_transfer_revert")
     * @Method("POST")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function revertAction(Request $request)
    {
        $id = (int)$request->get('id');

        if (!isset($id) || empty($id)) {
            return new JsonResponse(array(
                'code' => 400,
                'message' => 'Wrong input',
                'errors' => array('errors' => array('Id parameter ontbreekt of is leeg'))),
                400);
        }

        $em = $this->getDoctrine()->getManager();
        $file = $em->getRepository(CandidatesImport::class)->find($id);
        $fileService = $this->container->get('file.upload.service');


        $fileService->getAbsolutePath();
        if (!isset($file)) {
            return new JsonResponse(array(
                'code' => 400,
                'message' => 'Wrong input',
                'errors' => array('errors' => array('Id van file bestaat niet in database'))),
                400);
        }
        $file->setDeleted(1);
        $em->persist($file);
        $em->flush();

        $candidates = $em->getRepository(Candidates::class)->candidatesFileimportDelete($id);
        foreach ($candidates as $candidate) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($candidate);
        }
        $em->flush();

        return new JsonResponse(array(
            'code' => 200,
            'message' => 'success',
            'errors' => array('errors' => array(''))), 200);
    }


}