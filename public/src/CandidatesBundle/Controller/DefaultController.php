<?php

namespace CandidatesBundle\Controller;

use CandidatesBundle\Form\ImportExcelType;
use CandidatesBundle\Form\UpdateCandidateType;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use CandidatesBundle\Entity\Candidates;
use CandidatesBundle\Form\CreateCandidateType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validation;

class DefaultController extends Controller
{
    /**
     * List view of all active candidates
     *
     * @Route("/candidates", name="candidates_list", requirements={"page" = "\d+"}, defaults={"page" = "1"})
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->getRepository(Candidates::class)->filterCandidate($request, false);

        $adapter = new DoctrineORMAdapter($queryBuilder);
        $pagerfanta = new Pagerfanta($adapter);
        $pagerfanta->setMaxPerPage(30); // 10 by default

        $page = $request->query->get('page');
        if ($page) {
            if (is_numeric($page) && $page <= $pagerfanta->getNbPages()) {
                $pagerfanta->setCurrentPage($page);
            } else {
                return $this->redirect($this->generateUrl('candidates_list'));
            }
        }

        $formCreate = $this->createForm(CreateCandidateType::class);

        return $this->render('CandidatesBundle::candidates_list.html.twig', array(
            "candidates" => $pagerfanta->getCurrentPageResults(),
            'createForm' => $formCreate->createView(),
            'pagination' => $pagerfanta
        ));
    }

    /**
     * Create new candidate (ajax call)
     * @Route("/candidate/create", name="candidate_create")
     * @param Request $request
     * @return JsonResponse
     */
    public function createAction(Request $request)
    {

        $candidate = new Candidates();
        $form = $this->createForm(CreateCandidateType::class, $candidate);
        $form->handleRequest($request);
        $errors = array();

        if ($form->isSubmitted()) {
            $validator = $this->get('validator');
            $errorsValidator = $validator->validate($candidate);

            foreach ($errorsValidator as $error) {
                array_push($errors, $error->getMessage());
            }

            if (count($errors) == 0) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($candidate);
                $em->flush();

                return new JsonResponse(array(
                    'code' => 200,
                    'message' => 'kandidaat toegevoegd',
                    'errors' => array('errors' => array(''))),
                    200);
            }
        }

        return new JsonResponse(array(
            'code' => 400,
            'message' => 'error of form leeg',
            'errors' => array('errors' => $errors)),
            400);
    }

    /**
     * Updates candidate with requested id (Ajax Call)
     *
     * @Route("/candidate/update/{id}", name="candidate_update" )
     * @param Request $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(Request $request, $id)
    {

        $candidate = $this->getDoctrine()
            ->getRepository(Candidates::class)
            ->find($id);

        $formUpdate = $this->createForm(UpdateCandidateType::class, $candidate);
        $formUpdate->handleRequest($request);
        $errors = array();

        if ($formUpdate->isSubmitted()) {
            $validator = $this->get('validator');
            $errorsValidator = $validator->validate($candidate);

            foreach ($errorsValidator as $error) {
                array_push($errors, $error->getMessage());
            }

            if (count($errors) == 0) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($candidate);
                $em->flush();

                return new JsonResponse(array(
                    'code' => 200,
                    'message' => 'kandidaat geupdate',
                    'errors' => array('errors' => array(''))),
                    200);
            } else {
                return new JsonResponse(array(
                    'code' => 400,
                    'message' => 'error',
                    'errors' => array('errors' => $errors)),
                    400);

            }
        }

        return $this->render('@Candidates/modals/candidates_update_modal.html.twig', array(
            'updateForm' => $formUpdate->createView()
        ));

    }

    /**
     * Deletes candidate with requested id (AJAX post call)
     *
     * @Route("/candidate/delete", name="candidate_delete")
     * @Method("POST")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction(Request $request)
    {
        $id = (int)$request->get('id');

        if (!isset($id) || empty($id)) {
            return new JsonResponse(array(
                'code' => 400,
                'message' => 'Wrong input',
                'errors' => array('errors' => array('Id parameter ontbreekt of is leeg'))),
                400);
        }

        $em = $this->getDoctrine()->getManager();
        $candidate = $em->getRepository(Candidates::class)->find($id);

        if (!isset($candidate)) {
            return new JsonResponse(array(
                'code' => 400,
                'message' => 'Wrong input',
                'errors' => array('errors' => array('Id van kandidaat bestaat niet in database'))),
                400);
        }

        $candidate->setDeleted(1);
        $em->remove($candidate);
        $em->flush();

        return new JsonResponse(array(
            'code' => 200,
            'message' => 'success',
            'errors' => array('errors' => array(''))), 200);
    }


}
