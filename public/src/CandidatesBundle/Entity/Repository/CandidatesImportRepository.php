<?php

namespace CandidatesBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class CandidatesImportRepository extends EntityRepository
{
    public function findAllSortDate()
    {
        $q = $this->createQueryBuilder('f')->addOrderBy('f.id', 'desc');
        return $q->getQuery()->getResult();
    }

}