<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 15-Aug-17
 * Time: 16:23
 */

namespace CandidatesBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;

class CandidatesRepository extends EntityRepository
{
    /**
     *  allowed search categories
     */

    const SEARCH_CATEGORIES = array('country', 'name', 'email', 'city', 'province', 'skills', 'note', 'job');

    const CAT = array('country', 'name', 'email', 'city', 'province', 'skills', 'note', 'job', 'id', 'approached',
        'mobile', 'linkedinUrl', 'dateAdded', 'note');

    const SORT = array('asc', 'desc');

    public function candidatesFileimportDelete($id)
    {
        $q = $this->createQueryBuilder('c')->select()
            ->where('c.fileImportId = :id')->setParameter('id', $id);
        return $q->getQuery()->getResult();
    }

    public function filterCandidate(Request $request, $showDeleted)
    {
        $q = $this->createQueryBuilder('c');
        $q->select('c');
        if (!empty($request->get('s')) && $request->get('c')) {
            if (in_array($request->get('c'), self::SEARCH_CATEGORIES, true)) {
                $q->where('c.' . $request->get('c') . ' LIKE :s')->setParameter('s', '%' . $request->get('s') . '%');
            }
        }
        if (!empty($request->get('sort')) && $request->get('cat')) {
            if (in_array($request->get('cat'), self::CAT, true) && in_array($request->get('sort'), self::SORT)) {
                $q->addOrderBy('c.' . $request->get('cat'), $request->get('sort'));
            }
        } else {
            $q->addOrderBy('c.id', 'desc');
        }

        if ($showDeleted) {
            $q->andWhere('c.deleted = :deleted')->setParameter('deleted', 0);

        }

        return $q;
    }


}