<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 07-Aug-17
 * Time: 14:27
 */

namespace CandidatesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="candidates_file_import")
 * @ORM\Entity(repositoryClass="CandidatesBundle\Entity\Repository\CandidatesImportRepository")
 */
class CandidatesImport
{


    /**
     * @var UploadedFile
     */
    private $file;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @ORM\Column(type="integer", length=10, nullable=true)
     */
    private $size;

    /**
     * Timestamp of creation
     *
     * @ORM\Column(name="date_added", type="datetime")
     *
     * @Gedmo\Timestampable(on="create")
     */
    private $dateAdded;

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted", nullable=true, type="smallint", options={"default":0})
     */
    private $deleted = 0;


    /**
     * Timestamp of deletion
     *
     * @ORM\Column(name="date_deleted", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="change", field="deleted", value="1")
     */
    private $dateDeleted;

    /**
     * @var integer
     *
     * @ORM\Column(name="file_used", nullable=true, type="smallint", options={"default":1})
     */
    private $fileUsed = 1;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $errorMessage;



    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CandidatesImport
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return CandidatesImport
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return CandidatesImport
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     *
     * @return CandidatesImport
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set dateDeleted
     *
     * @param \DateTime $dateDeleted
     *
     * @return CandidatesImport
     */
    public function setDateDeleted($dateDeleted)
    {
        $this->dateDeleted = $dateDeleted;

        return $this;
    }

    /**
     * Get dateDeleted
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->dateDeleted;
    }

    /**
     * Set fileUsed
     *
     * @param integer $fileUsed
     *
     * @return CandidatesImport
     */
    public function setFileUsed($fileUsed)
    {
        $this->fileUsed = $fileUsed;

        return $this;
    }

    /**
     * Get fileUsed
     *
     * @return integer
     */
    public function getFileUsed()
    {
        return $this->fileUsed;
    }

    /**
     * Set errorMessage
     *
     * @param string $errorMessage
     *
     * @return CandidatesImport
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;

        return $this;
    }

    /**
     * Get errorMessage
     *
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }
}
