<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 10-9-17
 * Time: 13:32
 */

namespace CandidatesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="candidates_google_forms")
 */
class CandidatesGoogleForms
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\ManyToOne(targetEntity="Candidates", inversedBy="googleForm")
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\Column(name="path_excel", type="string", length=255, nullable=true)
     */
    private $pathExcel;

    /**
     * @ORM\Column(name="path_pdf", type="string", length=255, nullable=true)
     */
    private $pathPdf;

    /**
     * @ORM\Column(name="path_word", type="string", length=255, nullable=true)
     */
    private $pathWord;

    /**
     * @ORM\Column(name="path_image", type="string", length=255, nullable=true)
     */
    private $pathImage;

    /**
     * Timestamp of creation
     *
     * @ORM\Column(name="date_added", type="datetime")
     *
     * @Gedmo\Timestampable(on="create")
     */
    private $dateAdded;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pathExcel
     *
     * @param string $pathExcel
     *
     * @return CandidatesGoogleForms
     */
    public function setPathExcel($pathExcel)
    {
        $this->pathExcel = $pathExcel;

        return $this;
    }

    /**
     * Get pathExcel
     *
     * @return string
     */
    public function getPathExcel()
    {
        return $this->pathExcel;
    }

    /**
     * Set pathPdf
     *
     * @param string $pathPdf
     *
     * @return CandidatesGoogleForms
     */
    public function setPathPdf($pathPdf)
    {
        $this->pathPdf = $pathPdf;

        return $this;
    }

    /**
     * Get pathPdf
     *
     * @return string
     */
    public function getPathPdf()
    {
        return $this->pathPdf;
    }

    /**
     * Set pathWord
     *
     * @param string $pathWord
     *
     * @return CandidatesGoogleForms
     */
    public function setPathWord($pathWord)
    {
        $this->pathWord = $pathWord;

        return $this;
    }

    /**
     * Get pathWord
     *
     * @return string
     */
    public function getPathWord()
    {
        return $this->pathWord;
    }

    /**
     * Set pathImage
     *
     * @param string $pathImage
     *
     * @return CandidatesGoogleForms
     */
    public function setPathImage($pathImage)
    {
        $this->pathImage = $pathImage;

        return $this;
    }

    /**
     * Get pathImage
     *
     * @return string
     */
    public function getPathImage()
    {
        return $this->pathImage;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return CandidatesGoogleForms
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set user
     *
     * @param \CandidatesBundle\Entity\Candidates $user
     *
     * @return CandidatesGoogleForms
     */
    public function setUser(Candidates $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \CandidatesBundle\Entity\Candidates
     */
    public function getUser()
    {
        return $this->user;
    }

}
