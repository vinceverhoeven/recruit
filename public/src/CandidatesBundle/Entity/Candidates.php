<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 07-Aug-17
 * Time: 14:27
 */

namespace CandidatesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="candidates_user")
 * @ORM\Entity(repositoryClass="CandidatesBundle\Entity\Repository\CandidatesRepository")
 */
class Candidates
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", length=36)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="CandidatesGoogleForms", mappedBy="user")
     */
    private $googleForm;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="job", type="string", length=255, nullable=true)
     */
    private $job;

    /**
     * @var integer
     *
     * @ORM\Column(name="approached", type="integer", length=2, nullable=true, options={"default":0}))
     */
    private $approached;

    /**
     * @var string
     *
     * @ORM\Column(name="level", type="string", length=255, nullable=true)
     */
    private $level;

    /**
     * @var string
     *
     * @ORM\Column(name="skills", type="string", length=255, nullable=true)
     */
    private $skills;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=255, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="province", type="string", length=255, nullable=true)
     */
    private $province;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="linkedin_url", type="string", length=255, nullable=true)
     */
    private $linkedinUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="platform", type="string", length=255, nullable=true)
     */
    private $platform;

    /**
     * @var integer
     *
     * @ORM\Column(name="file_import_id", nullable=true, type="integer")
     */
    private $fileImportId;

    /**
     * @var string
     *
     * @ORM\Column(name="added_company", type="string", length=255, nullable=true)
     */
    private $addedCompany;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=255, nullable=true)
     */
    private $note;

    /**
     * Timestamp of creation
     *
     * @ORM\Column(name="date_added", type="datetime")
     *
     * @Gedmo\Timestampable(on="create")
     */
    private $dateAdded;

    /**
     * @var integer
     *
     * @ORM\Column(name="deleted", nullable=true, type="smallint", options={"default":0})
     */
    private $deleted = 0;


    /**
     * Timestamp of deletion
     *
     * @ORM\Column(name="date_deleted", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="change", field="deleted", value="1")
     */
    private $dateDeleted;

    /**
     * @var string
     *
     * @ORM\Column(name="crawled_url", nullable=true, type="string")
     */
    private $crawledUrl;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Candidates
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set job
     *
     * @param string $job
     *
     * @return Candidates
     */
    public function setJob($job)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return string
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set approached
     *
     * @param integer $approached
     *
     * @return Candidates
     */
    public function setApproached($approached)
    {
        $this->approached = $approached;

        return $this;
    }

    /**
     * Get approached
     *
     * @return integer
     */
    public function getApproached()
    {
        return $this->approached;
    }

    /**
     * Set level
     *
     * @param string $level
     *
     * @return Candidates
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set skills
     *
     * @param string $skills
     *
     * @return Candidates
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;

        return $this;
    }

    /**
     * Get skills
     *
     * @return string
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Candidates
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return Candidates
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Candidates
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set province
     *
     * @param string $province
     *
     * @return Candidates
     */
    public function setProvince($province)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return string
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Candidates
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set linkedinUrl
     *
     * @param string $linkedinUrl
     *
     * @return Candidates
     */
    public function setLinkedinUrl($linkedinUrl)
    {
        $this->linkedinUrl = $linkedinUrl;

        return $this;
    }

    /**
     * Get linkedinUrl
     *
     * @return string
     */
    public function getLinkedinUrl()
    {
        return $this->linkedinUrl;
    }

    /**
     * Set platform
     *
     * @param string $platform
     *
     * @return Candidates
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;

        return $this;
    }

    /**
     * Get platform
     *
     * @return string
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * Set fileImportId
     *
     * @param integer $fileImportId
     *
     * @return Candidates
     */
    public function setFileImportId($fileImportId)
    {
        $this->fileImportId = $fileImportId;

        return $this;
    }

    /**
     * Get fileImportId
     *
     * @return integer
     */
    public function getFileImportId()
    {
        return $this->fileImportId;
    }

    /**
     * Set addedCompany
     *
     * @param string $addedCompany
     *
     * @return Candidates
     */
    public function setAddedCompany($addedCompany)
    {
        $this->addedCompany = $addedCompany;

        return $this;
    }

    /**
     * Get addedCompany
     *
     * @return string
     */
    public function getAddedCompany()
    {
        return $this->addedCompany;
    }

    /**
     * Set note
     *
     * @param string $note
     *
     * @return Candidates
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set dateAdded
     *
     * @param \DateTime $dateAdded
     *
     * @return Candidates
     */
    public function setDateAdded($dateAdded)
    {
        if ($dateAdded instanceof \DateTime) {
            $this->dateAdded = $dateAdded;
        }

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \DateTime
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     *
     * @return Candidates
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set dateDeleted
     *
     * @param \DateTime $dateDeleted
     *
     * @return Candidates
     */
    public function setDateDeleted($dateDeleted)
    {
        if ($dateDeleted instanceof \DateTime) {
            $this->dateDeleted = $dateDeleted;
        }

        return $this;
    }

    /**
     * Get dateDeleted
     *
     * @return \DateTime
     */
    public function getDateDeleted()
    {
        return $this->dateDeleted;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->googleForm = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add googleForm
     *
     * @param \CandidatesBundle\Entity\CandidatesGoogleForms $googleForm
     *
     * @return Candidates
     */
    public function addGoogleForm(\CandidatesBundle\Entity\CandidatesGoogleForms $googleForm)
    {
        $this->googleForm[] = $googleForm;

        return $this;
    }

    /**
     * Remove googleForm
     *
     * @param \CandidatesBundle\Entity\CandidatesGoogleForms $googleForm
     */
    public function removeGoogleForm(\CandidatesBundle\Entity\CandidatesGoogleForms $googleForm)
    {
        $this->googleForm->removeElement($googleForm);
    }

    /**
     * Get googleForm
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGoogleForm()
    {
        return $this->googleForm;
    }

    /**
     * Set crawledUrl
     *
     * @param string $crawledUrl
     *
     * @return Candidates
     */
    public function setCrawledUrl($crawledUrl)
    {
        $this->crawledUrl = $crawledUrl;

        return $this;
    }

    /**
     * Get crawledUrl
     *
     * @return string
     */
    public function getCrawledUrl()
    {
        return $this->crawledUrl;
    }
}
