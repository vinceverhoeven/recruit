<?php

namespace CandidatesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use CandidatesBundle\Entity\Candidates;


class UpdateCandidateType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Volledige naam',
                'required' => true,
                'attr' => array()
            ))
            ->add('job', TextType::class, array(
                'label' => 'Vacature',
                'required' => true,
                'attr' => array(
                    'value' => 'Back-end developer'
                )
            ))
            ->add('level', TextType::class, array(
                'label' => 'Niveau',
                'attr' => array(
                    'value' => 'Junior'
                )
            ))
            ->add('skills', TextType::class, array(
                'label' => 'skills',
                'attr' => array(
                    'placeholder' => 'Bijv php, .net, ruby',
                    'value' => 'php symfony angularjs'
                )
            ))
            ->add('email', EmailType::class, array(
                'label' => 'Email',
                'attr' => array(
                    'value' => 'vinceverhoeven@hotmail.com'
                )
            ))
            ->add('mobile', TextType::class, array(
                'label' => 'Mobiel nummer',
                'attr' => array(
                    'value' => '0622624996'
                )
            ))
            ->add('city', TextType::class, array(
                'label' => 'Stad',
                'attr' => array(
                    'value' => 'Breda'
                )
            ))
            ->add('province', TextType::class, array(
                'label' => 'Provincie',
                'attr' => array(
                    'value' => 'Noord-Brabant'
                )
            ))
            ->add('country', TextType::class, array(
                'label' => 'Land',
                'attr' => array(
                    'value' => ''
                )
            ))
            ->add('linkedinUrl', TextType::class, array(
                'label' => 'Linkedin Url',
                'attr' => array(
                    'value' => 'https://www.linkedin.com/in/vinceverhoeven/'
                )
            ))
            ->add('addedCompany', TextType::class, array(
                'label' => 'Gekoppeld bedrijf',
                'attr' => array(
                    'value' => 'Google'
                )
            ))
            ->add('note', TextareaType::class, array(
                'label' => 'Notitie',
                'data' => 'Heeft momenteel geen interesse',
                'attr' => array()
            ))
            ->add('approached', RangeType::class, array(
                'data' => 0,
                'attr' => array(
                    'min' => 0,
                    'max' => 1,
                )
            ))
            ->add('save', SubmitType::class, array(
                    'label' => 'Opslaan',
                    'attr' => array()
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Candidates::class,
            'attr' => array(
                'novalidate' => 'novalidate',
                'id' => 'updateForm'
            )
        ));
    }

    public function getName()
    {
        return 'candidate_update';
    }
}















