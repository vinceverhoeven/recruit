<?php
/**
 * Created by PhpStorm.
 * User: vince
 * Date: 11-Aug-17
 * Time: 22:49
 */

namespace CandidatesBundle\Form;

use CandidatesBundle\Entity\CandidatesImport;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImportExcelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class, array(
                    'label' => 'upload')
            )->add('upload', SubmitType::class, array(
                    'label' => 'upload')
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CandidatesImport::class,
            'attr' => array('novalidate' => 'novalidate', 'id' => 'transferForm')
        ));
    }

    public function getName()
    {
        return 'candidate_import_excel';
    }
}