<?php

/*
 * (c) Vince Verhoeven <vinceverhoeven@hotmail.com>
 */


namespace RecruitersBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use RecruitersBundle\Entity\User;

class LoadUserData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('vinceverhoeven@hotmail.com');
        $user->setUsername('vince');
        $user->setPlainPassword('password');
        $user->setEnabled(1);
        $manager->persist($user);
        $manager->flush();
    }
}