<?php

namespace RecruitersBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class RecruitersBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
