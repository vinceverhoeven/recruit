$(document).ready(function () {
    /* ==== Delete Candidate ==== */
    $(".candidate-delete").click(function () {
        var id = $(this).attr('content');
        var url = location.origin + '/web/app_dev.php/candidate/delete';

        $.ajax({
            type: "POST",
            url: url,
            data: {id: id},
            success: function (result) {
                console.log(result);
                if (result.code === 200) {

                } else {

                }
            }
        });

    });


    /* ==== Create Candidate ==== */

    $("#createForm").submit(function (e) {

        e.preventDefault();
        var formSerialize = $(this).serialize();

        var url = location.origin + '/web/app_dev.php/candidate/create';
        $.ajax({
            type: "POST",
            url: url,
            data: formSerialize,
            success: function (result) {
                console.log(result);
                if (result.code === 200) {
                    // refresh current url to see candidate
                } else {

                }
            }
        });
    });


    /* ==== Transfer Candidate ==== */

    //GETS The transger form with all the data
    $("#addModalsButton").click(function () {
        var checkExistence = $("#transferForm")[0];
        if (!checkExistence) {
            var url = location.origin + '/web/app_dev.php/candidates/transfer';
            $.ajax({
                type: "GET",
                url: url,
                success: function (result) {
                    $("#importClient").append(result);
                }
            });
        }
    });

    $('body').on('click', '#exportCandidates', function () {
        $('#exportCandidates').prop('disabled', true);

        var url = location.origin + '/web/app_dev.php/candidates/transfer/export';
        $.ajax({
            type: "GET",
            url: url,
            success: function (result) {
                // download link
                var downLoadLink = location.origin + '/' + result.message;
                var dlif = $('<iframe/>', {'src': downLoadLink}).hide();
                //Append the iFrame to the context
                $('#exportCandidatesContainer').append(dlif);
                console.log(downLoadLink);
            }
        });
    });

    $('body').on('submit', '#transferForm', function (e) {
        // TODO disable submit button on send like exportCandidates

        console.log('transfer form');

        e.preventDefault();
        // var formSerialize = $(this).serialize();
        var url = location.origin + '/web/app_dev.php/candidates/transfer/import';
        var data = new FormData(this);
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            success: function (result) {
                // TODO show success notice and then this
                if (result.code === 200)
                    location.reload();
            }
            // TODO show errors if form empty or invalid file (ajax response)

        });
    });

    $('body').on('click', '.candidate-file-revert', function () {
        // TODO disable button on send like exportCandidates
        var id = $(this).attr('content');
        var url = location.origin + '/web/app_dev.php/candidates/transfer/revert';
        console.log(id, url);
        $.ajax({
            type: "POST",
            url: url,
            data: {id: id},
            success: function (result) {
                // TODO show success notice and then this
                if (result.code === 200)
                    location.reload();
            }
        });

    });

    /* ==== UPDATE Candidate ==== */

// GETS The form with all the data
// TODO when click on veranderen this gets the right form of the user. Then parse this into view (now its static 188)
    var url = location.origin + '/web/app_dev.php/candidate/update/188';
    $.ajax({
        type: "GET",
        url: url,
        success: function (result) {
        }
    });

    // will not work cuz listener is made before ajax result
    $("#updateForm").submit(function (e) {
        // TODO get id from view so when click on veranderen set a variable with id (now its static 188)
        e.preventDefault();
        var formSerialize = $(this).serialize();

        var url = location.origin + '/web/app_dev.php/candidate/update/188';
        $.ajax({
            type: "POST",
            url: url,
            data: formSerialize,
            success: function (result) {
                console.log(result);
                if (result.code === 200) {
                    // refresh current url to see candidate
                } else {

                }
            }
        });
    });
})
;