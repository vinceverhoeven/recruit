$(document).ready(function () {
    var colors = ['#74E7CF', '#75CCE7', '#E775A4', '#8EE777', '#F5DA5E'];

    $('.profile-pic').each(function() {
        $(this).css('background-color', colors[Math.floor(Math.random() * colors.length)]);
    });
});