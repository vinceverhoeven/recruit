<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170928131422 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE recruiters_user (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_7D29224A92FC23A8 (username_canonical), UNIQUE INDEX UNIQ_7D29224AA0D96FBF (email_canonical), UNIQUE INDEX UNIQ_7D29224AC05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE candidates_user (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, job VARCHAR(255) DEFAULT NULL, approached INT DEFAULT 0, level VARCHAR(255) DEFAULT NULL, skills VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, mobile VARCHAR(255) DEFAULT NULL, city VARCHAR(255) DEFAULT NULL, province VARCHAR(255) DEFAULT NULL, country VARCHAR(255) DEFAULT NULL, linkedin_url VARCHAR(255) DEFAULT NULL, platform VARCHAR(255) DEFAULT NULL, file_import_id INT DEFAULT NULL, added_company VARCHAR(255) DEFAULT NULL, note VARCHAR(255) DEFAULT NULL, date_added DATETIME NOT NULL, deleted SMALLINT DEFAULT 0, date_deleted DATETIME DEFAULT NULL, crawled_url VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE candidates_google_forms (id INT AUTO_INCREMENT NOT NULL, path_excel VARCHAR(255) DEFAULT NULL, path_pdf VARCHAR(255) DEFAULT NULL, path_word VARCHAR(255) DEFAULT NULL, path_image VARCHAR(255) DEFAULT NULL, date_added DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE candidates_file_import (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, path VARCHAR(255) DEFAULT NULL, size INT DEFAULT NULL, date_added DATETIME NOT NULL, deleted SMALLINT DEFAULT 0, date_deleted DATETIME DEFAULT NULL, file_used SMALLINT DEFAULT 1, errorMessage VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE webcrawler (id INT AUTO_INCREMENT NOT NULL, crawled_url VARCHAR(255) DEFAULT NULL, date_added DATETIME NOT NULL, date_updated DATETIME NOT NULL, number_crawled INT DEFAULT 0, log LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE webcrawler_content (id INT AUTO_INCREMENT NOT NULL, parsed_link VARCHAR(255) NOT NULL, number_parsed INT DEFAULT 0, date_added DATETIME NOT NULL, date_updated DATETIME NOT NULL, crawledUrl_id INT DEFAULT NULL, INDEX IDX_F1F85FF09BF7F28B (crawledUrl_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE candidates_google_forms ADD CONSTRAINT FK_F76E8289BF396750 FOREIGN KEY (id) REFERENCES candidates_user (id)');
        $this->addSql('ALTER TABLE webcrawler_content ADD CONSTRAINT FK_F1F85FF09BF7F28B FOREIGN KEY (crawledUrl_id) REFERENCES webcrawler (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE candidates_google_forms DROP FOREIGN KEY FK_F76E8289BF396750');
        $this->addSql('ALTER TABLE webcrawler_content DROP FOREIGN KEY FK_F1F85FF09BF7F28B');
        $this->addSql('DROP TABLE recruiters_user');
        $this->addSql('DROP TABLE candidates_user');
        $this->addSql('DROP TABLE candidates_google_forms');
        $this->addSql('DROP TABLE candidates_file_import');
        $this->addSql('DROP TABLE webcrawler');
        $this->addSql('DROP TABLE webcrawler_content');
    }
}
