# Vagrant PHP7 
# Configured by Vince Verhoeven vinceverhoeven@hotmail.com

A Vagrant LAMP setup running PHP7.

## What's inside?

- Ubuntu 14.04.3 LTS (Trusty Tahr)
- Vim, Git, Curl, etc.
- Apache
- PHP7 with some extensions
- MySQL 5.6
- Node.js with NPM
- Elastic Search 4.0
- Composer
- phpMyAdmin

## How to use

- Clone this repository into your project
- Run ``vagrant up``

- Password for phpmyadmin & mysql are `root`
- Application username `vince` password `password`
- app can be reached by http://192.168.100.100/web/app_dev.php
- phpMyAdmin can be reached by http://192.168.100.100/phpmyadmin/

- open console and run ``vagrant ssh``
- in vagrant box go to ``cd /var/www/html``
- next install packages ``composer install``
- next ``php bin/console doctrine:database:create``
- next ``php bin/console doctrine:migrations:migrate``
- next ``php bin/console doctrine:fixtures:load``


## When things not run, run the following
- (1) ``composer install``
- (2) ``php bin/console doctrine:migrations:migrate``
- (3) ``php bin/console doctrine:fixtures:load``

